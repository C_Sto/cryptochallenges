#include <iostream>
#include <string>
using namespace std;

int char2int(char input){
	if (input >= '0' && input <= '9')
		return input - '0';
	if (input >= 'A' && input <= 'F')
		return input - 'A' + 10;
	if (input >= 'a' && input <= 'f')
		return input - 'a' + 10;
	throw exception("bad string try again");
}
 
void hex2bin(const char* src, char* targ){
	while (*src && src[1]){
		*(targ++) = char2int(*src) * 16 + char2int(src[1]);
		src += 2;
	}
}


int main(){
	char* raw = new char;
	char* b64 = new char;
	
	char* hexStr = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
	hex2bin(hexStr, raw);
	cout <<"Hex: "<< hexStr << '\n';
	cout <<"Raw: "<< *raw << '\n';




	system("pause");

	return 0;
}