#include <iostream>
#include <vector>
#include <sstream>
#include "stringfuncs.h"
#include "challenges.h"

std::string str = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
std::string cmp = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";

void challenge1(){
		std::vector<unsigned char> vec = readHex(str);
		std::string s(vec.begin(), vec.end());
		std::string b64 = base64_encode(vec);
		std::cout << s << '\n';
		std::cout << "B64: " << b64 << '\n';
}