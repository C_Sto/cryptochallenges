#include "challenges.h"
#include <iostream>

#include "fileio.h"
#include "aes.h"
#include "stringfuncs.h"


void challenge10(){
	std::vector<unsigned char> ciphBytes = base64_decode(loadFileIntoString("challenges/challenge10.txt"));
	std::vector<unsigned char> iv;
	int blocksize = 128;
	while (iv.size() < blocksize/8){
		iv.push_back('\x0');
	}
	std::vector<unsigned char> decc = aes_cbc_decrypt(ciphBytes, "YELLOW SUBMARINE",iv,blocksize);
	std::string s(decc.begin(), decc.end());

	std::cout << s << '\n';
}