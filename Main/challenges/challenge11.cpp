#include "challenges.h"

#include <iostream>
#include <vector>
#include <random>
#include <ctime>
#include <functional>

#include "aes.h"
#include "oracles.h"
#include "blackboxes.h"



void challenge11(){
	std::cout << "A 0 indicates ECB, a 1 indicates CBC:\n";
	std::cout << aes_ecb_oracle(encryption_blackBox1) << '\n';
}