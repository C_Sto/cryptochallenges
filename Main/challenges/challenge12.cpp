#include "challenges.h"

#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <thread>
#include <chrono>


#include "blackboxes.h"
#include "aes.h"
#include "oracles.h"
#include "vectorfuncs.h"



void challenge12(){
	//detect the blocksize
	int blocksize = get_blocksize_oracle(encryption_blackBox_12);
	//make sure it's ecb
	if(!aes_ecb_oracle(encryption_blackBox_12))return;
	//get the ciphertext and turn it into chunks to work with
	std::vector<std::vector<unsigned char>> cipherChunks = chunker(encryption_blackBox_12({}), blocksize / 8);
	//plain will hold the plaintext we get.
	std::vector<std::vector<unsigned char>> solvedChunks(cipherChunks.size() + 1);
	solvedChunks[0] = { 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A' };
	std::vector<unsigned char> pad = solvedChunks[0];
	std::vector<unsigned char> chunk;
	std::vector<unsigned char> solvedChunk;
	std::vector<unsigned char> comparison_block;
	std::vector<unsigned char> gen_block;
	if (aes_ecb_oracle(encryption_blackBox_12)){
		std::cout << "ECB Detected. Trying Byte at a time ECB Decryption...\n";
		//iterate over each chunk
		for (int i = 0; i < cipherChunks.size(); i++){
			chunk = cipherChunks[i];
			pad = solvedChunks[0];
			solvedChunk = solvedChunks[i];
			//for each unknown byte in the block (shortblock length+1)
			for (int j = 15; j > -1; j--){
				pad.erase(pad.begin());
				solvedChunk.erase(solvedChunk.begin());
				comparison_block = chunker(encryption_blackBox_12(pad), blocksize / 8)[i];
				//compare it to the generated ciphertext for each character
				for (int k = 0; k < 255; k++){					
					solvedChunk.push_back((unsigned char)k);
					if (isprint((unsigned char)k))					{
						std::this_thread::sleep_for(std::chrono::milliseconds(2));
						std::cout << (unsigned char)k << "\b";					}
					gen_block = chunker(encryption_blackBox_12(solvedChunk), blocksize / 8)[0];
					if (gen_block == comparison_block){
						solvedChunks[i + 1].push_back((unsigned char)k);
						std::cout << (unsigned char)k;
						break;
					}
					solvedChunk.pop_back();
				}
			}
		}
	}
	//recombine the chunks for output
	solvedChunks.erase(solvedChunks.begin());
	std::string s;
	for (std::vector<unsigned char> chunk : solvedChunks){
		std::string ss(chunk.begin(), chunk.end());
		s += ss;

	}
}