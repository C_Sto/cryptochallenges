#include "challenges.h"

#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <time.h>

#include "aes.h";
#include "stringfuncs.h"
#include "vectorfuncs.h"

struct user{
	std::string email;
	std::string uid;
	std::string role;
};

std::map<std::string, std::string> parse_cookie(std::string in){
	std::map<std::string, std::string> m;
	std::stringstream inStream(in);
	std::string segment;
	std::vector<std::string> seglist;
	while (std::getline(inStream, segment, '&')){
		m.emplace(
			segment.substr(0, segment.find('=')),
			segment.substr(segment.find('=')+1, segment.size())
			);
		seglist.push_back(segment);
	}
	return m;
}

std::string profile_for(std::string input){
	std::string ret;
	std::string badChar = "&=";
	for (int i = 0; i < badChar.length(); i++){
		input.erase(std::remove(input.begin(),input.end(),badChar[i]),input.end());
	}
	ret = "email="+input+"&uid=10&role=user";
	return ret;
}

std::vector<unsigned char> encrypt_profile(std::string cookie, std::vector<unsigned char> key){
	return aes_ecb_encrypt(cookie, key,128);;
}

std::vector<unsigned char> random_aes_key(int bytes){
	srand(time(nullptr));
	std::vector<unsigned char> ret;
	for (int i = 0; i < bytes; i++){
		ret.push_back(rand() % 255);
	}
	return ret;
}

std::map<std::string, std::string> decrypt_and_parse_profile(std::vector<unsigned char> in,std::vector<unsigned char> key){
	std::vector<unsigned char> v = aes_ecb_decrypt(in, key, 128);
	std::string s(v.begin(), v.end());
	return parse_cookie(s);

}

void challenge13(){
	std::vector<unsigned char> key = random_aes_key(16);
	//padding of sie 11 is required to push the 'admin' role into the last block
	// find admin ciphertext by manufacturing 'admin' block
	// a,d,m,i,n,\x11,\x11 etc
	std::vector<unsigned char> adminPlainBlock({ '\x11', '\x11',
												'\x11', '\x11', '\x11', '\x11',
												'\x11', '\x11', '\x11', '\x11',
												'a', 'd', 'm', 'i',
												'n', '\v', '\v', '\v',
												'\v', '\v', '\v', '\v',
												'\v', '\v', '\v', '\v' });
	std::vector<unsigned char> acb(encrypt_profile(profile_for(std::string(adminPlainBlock.begin(), adminPlainBlock.end())), key));
	std::vector<unsigned char> adminCryptBlock(16);
	std::map<std::string, std::string> m2;
	for (int j = 16; j<32; j++){
		adminCryptBlock[j-16] = acb[j];
	}
	//now find the length needed for 'admin' to be pushed onto the last block (it's 13)
	
	//encrypt with length required - replace last block with admin block, be happy with admin status
	std::map<std::string, std::string> m;

	std::vector<unsigned char> pad = { 'f','r','e','d','@','n','1','2','3','.','c','o','m' };
	std::vector<unsigned char> v(encrypt_profile(profile_for(std::string(pad.begin(), pad.end())), key));
	//look at before fuckery
	v[v.size() - 1] = ~v[v.size() - 1];
	m = decrypt_and_parse_profile(v, key);
	int count = 0;
	for (int i = v.size() - 16; i < v.size(); i++){
		v[i] = adminCryptBlock[count];
		count++;
	}
	m = decrypt_and_parse_profile(v, key);
	std::cout << "User priv: " << m.at("role")<<'\n';
}