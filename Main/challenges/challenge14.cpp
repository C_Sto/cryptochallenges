#include "challenges.h"
#include "blackboxes.h"
#include <random>
#include <time.h>
#include <iostream>

#include "vectorfuncs.h"
#include "stringfuncs.h"
#include "oracles.h"
#include "aes.h"

void challenge14(){
	srand(time(NULL));

	int randPadSize = rand() % 16;
	//same as 12, but random pad before - generated at challenge start and maintend throughout challenge (constant random block prepend)

	//estblish blocksize
	int blockSize = get_blocksize_oracle(encryption_blackBox_14, randPadSize);

	//make sure it's ECB
	std::vector<unsigned char> bigPad;
	for (int i = 0; i < 100; i++){
		bigPad.push_back('A');
	}
	if (is_ecb_mode(encryption_blackBox_14(bigPad,randPadSize),blockSize*8)){
		printf("ECB detected, proceeding with decryption...\n");
	}
	else{
		printf("Doesn't look like ECB, exiting\n");
		return;
	}

	//insert known pad of 3 x blocksize, 
	std::vector<unsigned char> knownPad(blockSize * 3);
	for (int i = 0; i < blockSize * 3; i++){
		knownPad[i] = 'A';
	}

	std::vector<unsigned char> cipher = encryption_blackBox_14(knownPad, randPadSize);
	//remove a char until there is no block repeats
	int count = 0;
	while (has_block_repeats(cipher, blockSize)){
		knownPad.pop_back();
		cipher = encryption_blackBox_14(knownPad, randPadSize);
		count++;
	}
	//we've gone one too far, decrement by one
	count--;
	
	//continue as challenge 12 BUT prepending enough characters to make a dud block, and starting from the second block instead of first.
	std::vector<unsigned char> dudPrepend(blockSize - count);
	for (int i = 0; i < blockSize - count; i++){
		dudPrepend[i] = '\x00';
	}

	cipher = encryption_blackBox_14(dudPrepend, randPadSize);
	std::vector<std::vector<unsigned char>> cipherChunks = chunker(cipher, blockSize);
	std::vector<std::vector<unsigned char>> solvedChunks(cipherChunks.size() + 1);
	solvedChunks[0] = { 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A' };
	std::vector<unsigned char> chunk;
	std::vector<unsigned char> pad;
	std::vector<unsigned char> solvedChunk;
	std::vector<unsigned char> comparison_block;
	std::vector<unsigned char> gen_block;
	//iterate over each chunk, starting with the second, to ignore the random prepend
	for (int i = 1; i < cipherChunks.size(); i++){
		chunk = cipherChunks[i];
		pad = solvedChunks[0];
		solvedChunk = solvedChunks[i-1];
		for (int j = 0; j < blockSize; j++){
			pad.erase(pad.begin());
			solvedChunk.erase(solvedChunk.begin());
			comparison_block = chunker(encryption_blackBox_14(join_Vectors(dudPrepend,pad), randPadSize), blockSize)[i];
			//compare it to the generated ciphertext for each cahracter
			for (int k = 0; k < 255; k++){
				solvedChunk.push_back((unsigned char)k);
				std::vector<std::vector<unsigned char>> chunksTest = chunker(encryption_blackBox_14(join_Vectors(dudPrepend, solvedChunk), randPadSize), blockSize);
				gen_block = chunksTest[1];
				if (gen_block == comparison_block){
					solvedChunks[i].push_back((unsigned char)k);
					std::cout << (unsigned char)k;
					break;
				}
				solvedChunk.pop_back();
			}
		}

	}
}

