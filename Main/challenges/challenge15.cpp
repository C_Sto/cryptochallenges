#include "challenges.h"

#include <iostream>
#include <vector>
#include <string>

#include "vectorfuncs.h"


void challenge15(){
	try{
		std::vector<unsigned char> testVec = PKCS_7_unpad({ 'I', 'C', 'E', ' ', 'I', 'C', 'E', ' ', 'B', 'A', 'B', 'Y', '\x04', '\x04', '\x04', '\x04' });
		std::vector<unsigned char> testVec2 = PKCS_7_unpad({ 'I', 'C', 'E', ' ', 'I', 'C', 'E', ' ', 'B', 'A', 'B', 'Y', '\x05', '\x05', '\x05', '\x05' });
		std::vector<unsigned char> testVec3 = PKCS_7_unpad({ 'I', 'C', 'E', ' ', 'I', 'C', 'E', ' ', 'B', 'A', 'B', 'Y', '\x01', '\x02', '\x03', '\x04' });
	}
	catch (char* e){
		printf("Caught error: ");
		std::cout << e;
	}
}