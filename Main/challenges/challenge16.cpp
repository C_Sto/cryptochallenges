#include "challenges.h"

#include "vectorfuncs.h"
#include "aes.h"

#include <vector>
#include <string>
#include <iostream>


std::vector<unsigned char> func1(std::string input, std::vector<unsigned char> key, std::vector<unsigned char> IV){
	std::string plaintext;
	//filter input
	std::string filtered;
	for (unsigned char c : input){
		if (c == ';' || c == '=')continue;
		filtered += c;
	}
	//prepend data
	std::string prepend = "comment1=cooking%20MCs;userdata=";
	plaintext = prepend + filtered;
	//append data
	std::string append = ";comment2=%20like%20a%20pound%20of%20bacon";
	plaintext = plaintext + append;
	//turn it into a vector for padding
	std::vector<unsigned char> plaintextVector = string_to_vector(plaintext);
	//pad to 16 byte (pkcs pad)
	std::vector<unsigned char> paddedPlaintextVector = PKCS_7_pad(plaintextVector,16);
	//encrypt, and return
	return aes_cbc_encrypt(paddedPlaintextVector, key, IV, 128);
}

bool func2(std::vector<unsigned char> input,std::vector<unsigned char> key, std::vector<unsigned char> iv){
	std::vector<unsigned char> plain = aes_cbc_decrypt(input, key, iv, 128);
	std::string plainString(plain.begin(), plain.end());

	return plainString.find(";admin=true;")!=std::string::npos;

}


void challenge16(){
	//generate random key
	std::vector<unsigned char> key = random_aes_key();
	//generate random IV
	std::vector<unsigned char> IV = random_aes_key();
	//make two blocks we know the content of, first one will be garbled, second will change on demand
	std::vector<unsigned char> cipherText = func1({ 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A',
													'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 
													}, key, IV);
	//put the desired text in a vector to work with it easily
	std::vector<unsigned char> adminBlock = string_to_vector(";admin=true;");
	int count = 0;
	//start at the first user controlled block, since we know the plaintext
	// this could be done autonomously by looking for a correct change state, and starting from the start
	// (look 16 bytes ahead, if that value is what we expect, go for it, otherwise revert change, and pick the next slot)
	for (int j = 32; j < cipherText.size()&&count<adminBlock.size(); j++){
		try{
			cipherText[j] = (unsigned char)'A' ^ (unsigned char)cipherText[j] ^ (unsigned char)adminBlock[count];
			count++;
			bool plainText = func2(cipherText, key, IV);
		}
		catch (char* exception){
		}
	}
	if (func2(cipherText, key, IV)) { std::cout << "win\n"; }
	else{std::cout << "Lose!\n";}

}