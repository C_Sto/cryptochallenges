#include "challenges.h"

#include "vectorfuncs.h"
#include "aes.h"
#include "stringfuncs.h"

#include <vector>
#include <string>
#include <iostream>
#include <random>

std::vector<unsigned char> pad_and_encrypt_random_string(std::vector<unsigned char> key, std::vector<unsigned char> iv){
	std::string strings[] = { "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
		"MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
		"MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
		"MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
		"MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
		"MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
		"MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
		"MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
		"MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
		"MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93" };
	std::string selected = strings[rand() % 9];
	std::vector<unsigned char> padded =PKCS_7_pad(string_to_vector(selected),16);
	return aes_cbc_encrypt(padded, key, iv, 128);
}
bool decrypt_and_check_padding(std::vector<unsigned char> in, std::vector<unsigned char> key,std::vector<unsigned char> iv){
	std::vector<unsigned char> decrypted = aes_cbc_decrypt(in, key, iv, 128);
	try{
		std::vector<unsigned char> unpadded = PKCS_7_unpad(decrypted);
	}
	catch(char* c){
		return false;
	}
	return true;
}
void challenge17(){
	//set key
	std::cout << "Setting random key and IV." << '\n';
	std::vector<unsigned char> key = random_aes_key();
	std::vector<unsigned char> iv = random_aes_key();
	bool previousSolved = false;
	//set block size - could do this programattically with prior funcs, cbf
	int blockSize = 16;
	//set random string to encrypt and pad - we use enc as the 'cookie' throughout
	std::cout << "Getting ciphertext: \n";
	std::vector<unsigned char> enc = pad_and_encrypt_random_string(key, iv);
	std::cout << vector_to_string(enc) << '\n';
	//decryption logic starts here. Add the IV to the start, as it's a CBC algorithm (prev[15]^(block[0][15] ^ 1 ^ [?] = 1)) gives us the plaintext at ?
	std::vector<unsigned char> iv_enc = join_Vectors(iv, enc);
	//chunk the blocks out, for easier management
	std::vector<std::vector<unsigned char>> chunks = chunker(iv_enc,16);
	//create plaintext chunked container from enc - might use this later to make it look h4x in the terminal
	std::vector<std::vector<unsigned char>> plainChunks = chunker(enc,16);
	//decrypt the blocks one by one
	std::cout << "Starting cracking.." << '\n';
	for (int i = 1; i < chunks.size(); i++){
		//in the case of the last block, if there is padding (there should be, its why this works) there is a chance the byte we try is going to accidently solve it.
		previousSolved = false;
		//we are dealing with 2 blocks at a time, block - 1 is our modified block
		std::vector<unsigned char> ciph = chunks[i];
		std::vector<unsigned char> mod = chunks[i - 1];
		std::vector<unsigned char> plain = plainChunks[i - 1];
		//break each byte
		for (int j = 0; j < ciph.size(); j++){
			//remember the character in the crypto
			unsigned char cryptChar = chunks[i-1][blockSize - j - 1];
			//set the known bytes
			for (int l = 0; l < j; l++){
				mod[blockSize - 1 - l] = chunks[i-1][blockSize-l-1] ^ plain[blockSize - l-1] ^ (unsigned char)(j+1);
			}
			//cycle the options
			for (unsigned char k = 0; k < 255; k++){
				mod[blockSize - j - 1] = cryptChar ^ k ^ (j + 1);
				//test for padding error
				if (decrypt_and_check_padding(join_Vectors(mod, ciph), key, iv)){
					//found it, all your base are belong to us, next byte
					plain[blockSize - 1 - j] = k;
					previousSolved = true;
					break;
				}
				previousSolved = false;
			}
			//this should only be reached on the last block, if and only if the first(last) byte 'accidently' completes the plaintext padding, eg.
			//		if the plaintext has a padding of x05, turning the last byte into x05 will complete it, when we are expecting x01, and things will get confusing.
			if (!previousSolved){ 
				j -= 2;
			}
		}
		plainChunks[i - 1] = plain;
		std::vector<unsigned char> joined = join_chunks(plainChunks);
		for (std::vector<unsigned char> v : plainChunks){
			std::vector<unsigned char> dc = base64_decode(vector_to_string(v));
			for (unsigned char c : dc){
				std::cout << c;
			}
		}
		std::cout << '\n';
	}
	std::cout << '\n';

	std::cout << "Plaintext after b64 decode: " << '\n' << '\n';
	std::cout << vector_to_string(base64_decode(vector_to_string( PKCS_7_unpad(join_chunks(plainChunks))))).substr(6) << '\n'<< '\n';

}