#include "challenges.h"

#include "vectorfuncs.h"
#include "aes.h"
#include "stringfuncs.h"

#include <vector>
#include <iostream>


void challenge18(){
	std::vector<unsigned char> key = string_to_vector("YELLOW SUBMARINE");
	std::vector<unsigned char> cipher = base64_decode("L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==");
	long nonce = 0;

	std::cout<<vector_to_string( aes_ctr_decrypt(cipher, key))<<'\n';


}