#include "challenges.h"

#include "fileio.h"
#include "stringfuncs.h"
#include "aes.h"
#include "vectorfuncs.h"
#include "xor.h"


#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>


void challenge19(){
	//load files and set key
	std::vector<unsigned char> key = random_aes_key();
	std::vector<std::vector<unsigned char>> cipherList;
	for (std::string s : loadFile("challenges/challenge19.txt")){
		cipherList.push_back(aes_ctr_encrypt(base64_decode(s),key,100));
	}
	//interactive! see theory below. Basically breaking one time pad re-use.
	bool quit = false;
	std::string input;
	int index = 0;
	int vnum = 0;
	//print the ciphertexts, so we can pick one to try out - initially look for similar occurences of bytes in the same index - indicates the same plaintext byte is there
	for (std::vector<unsigned char> v : cipherList){
		std::string str = vector_to_string(v);
		//strip non-printables, otherwise things get messy
		str = strip_non_print(str);
		printf("%d:\t%s\n", vnum, str.c_str());
		vnum++;
	}
	//continue until user indicates they want to exit
	while (!quit){
		//ask for the inex of the plaintext message to guess. This is where we generate our guessed key from
		printf("Set plaintext number to try guess (-1 to quit): \n");
		std::getline(std::cin, input);
		std::stringstream inStream(input);
		if (!(inStream >> index)){printf("Error\n"); continue;}
		if (index == -1)break;
		//user enters what they think the plaintext of the selected message is, if the other messages decrypt to the point they guessed, it's right
		printf("Enter plaintext to try:\n");
		std::getline(std::cin,input);
		printf("Trying..\n");
		int indx = 0;
		//display all the results
		for (std::vector<unsigned char> v : cipherList){
			//ciphertext ^ guess = key
			std::vector<unsigned char> tryKey = xor_longest(string_to_vector(input), cipherList[index]);
			//ciphertext ^ key = plaintext
			std::cout<<indx <<":\t"<<strip_non_print(vector_to_string(xor_longest(tryKey, v)))<<'\n';
			indx++;
		}
	}

	printf("End\n");
}