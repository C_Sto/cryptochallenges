#include "challenges.h"
#include <vector>
#include <iostream>

#include "stringfuncs.h"
#include "xor.h"



void challenge2(){
	std::string str1 = "1c0111001f010100061a024b53535009181c";
	std::string str2 = "686974207468652062756c6c277320657965";
	std::string check = "746865206b696420646f6e277420706c6179";

	std::vector<unsigned char> bytes1 = readHex(str1);
	std::vector<unsigned char> bytes2 = readHex(str2);
	std::vector<unsigned char> bytes3 = xor_shortest(bytes1, bytes2);
	std::vector<unsigned char> bytesCheck = readHex(check);
	std::string s(bytes3.begin(), bytes3.end());
	std::string ss(bytesCheck.begin(), bytesCheck.end());
	std::cout << "S:" << s << '\n';
	std::cout << "Check: " << ss.compare(s) << '\n';
}