#include "challenges.h"

#include "fileio.h"
#include "aes.h"
#include "stringfuncs.h"
#include "vectorfuncs.h"
#include "xor.h"
#include "mapfuncs.h"

#include<iostream>
#include <vector>
#include <map>

void challenge20(){
	//load files and set key
	std::vector<unsigned char> key = random_aes_key();
	std::vector<std::vector<unsigned char>> cipherList;
	for (std::string s : loadFile("challenges/challenge20.txt")){
		cipherList.push_back(aes_ctr_encrypt(base64_decode(s), key, 100));
	}
	//find shortest, store length
	int repeatLen = INT_MAX;
	for (std::vector<unsigned char> v : cipherList){
		if (v.size() < repeatLen)repeatLen = v.size();
	}
	//concatenate all at shortest length
	std::vector<unsigned char> concVec;
	for (std::vector<unsigned char> v : cipherList){
		concVec.insert(concVec.end(), v.begin(), v.begin()+repeatLen);
	}

	//BREAK REPEATING KEY XOR (Challenge 6) (copy pasted code from earlier challenge, don't hate me)

	//break into blocks of keysise length
	std::vector<std::vector<unsigned char>>chunks = chunker(concVec, repeatLen);
	//transpose the things
	std::vector<std::vector<unsigned char>>transposedChunks = transposeChunks(chunks);
	//solve each block as a single byte xor
	std::string pw;
	std::cout << "Cracking first section of keystream, " << repeatLen << " bytes long..." << '\n';
	double ch = 0;
	for (std::vector<unsigned char>v : transposedChunks){
		std::map<unsigned char, double> m = singleByte_xor_ScoreMap(v);
		unsigned char c = getLowestKey(m);
		pw += c;
		if (isprint(c))std::cout << c << " " << (ch / (double)repeatLen) * 100 << "% Done.." << '\r';
		ch++;
	}
	std::cout << '\n' << "Done. First section of keystream is: " << pw << '\n';
	std::cout << "Decrypted text is: " << '\n';
	std::string ciphText(concVec.begin(), concVec.end());
	std::vector<unsigned char> pl = repeatingKeyXOr(ciphText, pw);
	for (std::vector<unsigned char> chunk : chunker(pl,repeatLen)){
		std::cout << std::string(chunk.begin(), chunk.end()) << '\n';
	}

	std::cout << '\n';
}