#include "challenges.h"

#include "random.h"

#include <iostream>
#include <ctime>
#include <Windows.h>
#include <random>

unsigned int get_Random(){
	srand(std::time(0));
	//wait between 40 and 1000 seconds
	int rnum = (rand()%(100-40))+40;
	Sleep(rnum*1000);
	//seed MT with timestamp
	unsigned int t = time(0);
	MT19937 mt = MT19937(t);
	std::cout << "Random number picked, crack me if you can!:\t" << t << '\n';
	//wait 40-1000 seconds again
	rnum = (rand() % (100 - 40)) + 40;
	Sleep(rnum * 1000);
	//return output of RNG
	unsigned int num = mt.get_number();
	
	return num;
}

unsigned int crack_RNG(unsigned int firstOutput){
	//set upper limit
	//since we know how the RNG is seeded, upper limit will be current time
	int upper = time(0);
	//set lower limit
	//we assume worst case of 2000 seconds, so our lower limit will be upper-2000
	int lower = upper - 2000;
	//cycle through each value from lower to upper, seed our own RNG with it and get the value, check for match
	for (int i = lower; i < upper; i++){
		MT19937 mt = MT19937(i);
		if (mt.get_number() == firstOutput)return i;
	}
	return 0;
}

void challenge22(){

	unsigned int v = crack_RNG(get_Random());
	printf("RNG Value cracked, answer is:\t\t\t%d\n", v);
}