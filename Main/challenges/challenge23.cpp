#include "challenges.h"

#include "random.h"

#include <ctime>
#include <iostream>
/* Temper function =
y = y ^ (y >> 11); //U
y = y ^ ((y << 7) & (2636928640)); //S
y = y ^ ((y << 15) & (4022730752)); //T
y = y ^ (y >> 18); //L
return y;
*/

unsigned int undo_L(unsigned int input){
	unsigned int end14 = input >> 18;
	return end14 ^ input;
}

unsigned int undo_T(unsigned int input){
	unsigned int first17 = input << 15;
	return input ^ (first17 & 0xefc60000);
}

unsigned int undo_S(unsigned int input){
	unsigned int a = input << 7;
	unsigned int b;
	for (int i = 0; i < 4; i++){
		b = input ^ (a & 0x9d2c5680);
		a = b << 7;
	}
	return input ^ (a & 0x9d2c5680);
}

unsigned int undo_U(unsigned int input){
	unsigned int a = input >> 11;
	unsigned int b = input ^ a;
	a = b >> 11;
	return a ^ input;
}

unsigned int undo_tempering(unsigned int input){
	input = undo_L(input);
	input = undo_T(input);
	input = undo_S(input);
	input = undo_U(input);
	return input;
}
void challenge23(){
	//get 624 values from 'random'
	std::cout << "Creating new rng with \"difficult to guess\" seed..\n";
	MT19937 mt = MT19937(time(0));
	unsigned int ary[624];
	std::cout << "Getting 624 outputs, and untempering them to get original state..\n";
	for (int i = 0; i < 624; i++){
		ary[i] = mt.get_number();
		std::cout << "Output from RNG["<<i+1<<"]:\t"<< ary[i] << '\t';
		//untemper them
		{
			ary[i] = undo_tempering(ary[i]);
		}
		std::cout <<"Untempered value:\t"<< ary[i] << '\r';
	}
	std::cout << "\nRNG state cloned, testing for matches..\n\n";
	//splice them into new instance of 'random'
	MT19937 mt2 = MT19937();
	mt2.splice(ary);
	//check for same numbers between the two
	for (int i = 0; i < 10; i++){
		std::cout <<"Original["<<i+1<<"]:\t"<< mt.get_number() << "\n" <<"Copied  ["<<i+1<<"]:\t"<< mt2.get_number() << "\n\n";
	}

}