#include "challenges.h"

#include <vector>
#include <iostream>
#include <time.h>
#include <string>

#include "random.h"
#include "vectorfuncs.h"
#include "aes.h";

std::vector<unsigned char> mt_enc(std::vector<unsigned char> in,unsigned int key){
	std::vector<unsigned char> ret;

	MT19937 mt = MT19937(key);

	unsigned char c = mt.get_number();

	for (unsigned char c : in){
		ret.push_back(c^mt.get_number()%255);
	}
	return ret;

}

std::vector<unsigned char> mt_dec(std::vector<unsigned char> in, unsigned int key){
	return mt_enc(in, key);
}

std::string generate_token(MT19937 &mt){
	std::string ret;
	for (int i = 0; i < 20; i++){
		unsigned char c = (unsigned char) mt.get_number() % (91 - 65) + 65;
		ret += c;
	}
	return ret;
}

std::string generate_token(){
	return generate_token(MT19937(time(NULL)));
}

std::string generate_token(long input){
	return generate_token(MT19937(input));
}

void challenge24(){
	srand(time(NULL));
	//set up first test
	//known (end) plain section
	std::vector<unsigned char> plain = string_to_vector("AAAAAAAAAAAA");
	int aLen = plain.size();
	//random random section
	std::vector<unsigned char> rsec;
	for (int i = 0; i < rand() % 100; i++){
		rsec.push_back(rand() % 255);
	}
	plain = join_Vectors(rsec, plain);
	//gen the key (16 bits, super secure, right?)
	int key = rand() & 0xFFFF;
	//encrypt
	std::vector<unsigned char> enc = mt_enc(plain, key);
	//find the key, based on ciphertext

	//I guess we could cycle each possible key (bruteforce) until the end of the recovered text is what we know. There might be a better way, but 16 bits is 'trivial' to brute.
	int bruteKey;
	for (bruteKey = 0; bruteKey < 0x10001; bruteKey++){
		std::string testStr = vector_to_string(mt_dec(enc, bruteKey)).substr(enc.size() - aLen, aLen);
		if (testStr == "AAAAAAAAAAAA") break;
	}
	if (bruteKey == 0x10001){ std::cout << "DIDNT FIND KEY, ABORT\n"; system("pause"); return; }
	else
	std::cout <<"Found key: " << bruteKey <<'\n';

	//section two - get 'password reset string' and clone the RNG
	std::cout << "Second part, for beating a random password token: \n";
	//as above - generating token from system time some days ago
	std::string token = generate_token(MT19937(time(NULL) - ((rand() % 404800)+400000)));
	std::cout << "HERE IS OUR TOTALLY RANDOM TOKEN: " << token<<'\n';
	//if we guess the seed, we could generate our own tokens, probably not ideal.
	//set our lower bound
	unsigned long guess = time(NULL);
	int count = 0;
	printf("Searching all possible system time seeds...\n");
	long timestart = time(NULL);
	for (guess; guess; guess--){
		//check every 50000 guesses, hopefully speedup
		if (guess % 50000 == 0){
			unsigned long perSecond = 0;
			long timeNow = time(NULL);
			if (timeNow - timestart != 0)
				perSecond = (long)(count / (timeNow - timestart))/1000;
			std::cout << perSecond << "k p/s\r";
		}
		//when we do find the right seed, clone the RNG, we now have the same series of values as that of the 'token' provider, can create tokens at will.
		if (generate_token(guess) == token){
			long distance = time(NULL) - guess;
			int days = (((distance / 60) / 60) / 24);
			std::cout << "\nToken seed is:\t" << guess << " " << '\n';
			std::cout << "For comparison:\t" << time(NULL) << " is the current time.\n";
			std::cout << " Which is " << days << " days ago, roughly\n";
			std::cout << "Try some of these tokens for password reset >:D\n";
			MT19937 mt = MT19937(guess);
			for (int i = 0; i < 10; i++){
				std::cout << generate_token(mt) << '\n';
			}
			break;
		}
		count++;
	}

	return;

}