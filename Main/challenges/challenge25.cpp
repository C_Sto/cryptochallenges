#include "challenges.h"

#include "vectorfuncs.h"
#include "aes.h"
#include "fileio.h"
#include "stringfuncs.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

static std::vector<unsigned char> key = random_aes_key();

std::vector<unsigned char> attack_edit(std::vector<unsigned char> ciphertext, long offset, std::vector<unsigned char> newtext){
	return aes_ctr_edit(ciphertext, key, offset, newtext);
}

std::vector<unsigned char> prepare_ciphertext(){
	std::vector<unsigned char> ciph = base64_decode(loadFileIntoString("challenges/challenge25.txt"));
	std::vector<unsigned char> test = aes_ecb_decrypt(ciph, "YELLOW SUBMARINE", 128);
	test = aes_ctr_encrypt(test, key);
	return test;
}

void challenge25(){
	//theory - change entire plaintext to known value, XOR encrypted known value with ciphertext to get keystream, XOR keystream with ciphertext to get plaintext
	//tl;dr = known_plain ^ known_cipher ^ cipher = plaintex
	std::vector<unsigned char> cipherText = prepare_ciphertext();
	std::vector<unsigned char> aText;
	//create enough known characters
	for (int i = 0; i < cipherText.size(); i++){
		aText.push_back('A');
	}
	//edit with known value
	std::vector<unsigned char> enc_As = attack_edit(cipherText, 0, aText);
	//perform xor operations, send to screen
	for (int i = 0; i < aText.size(); i++){
		std::cout << (unsigned char)(enc_As[i] ^ (unsigned char)'A' ^ cipherText[i]);
	}

}