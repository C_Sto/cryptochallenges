#include "challenges.h"

#include "aes.h"
#include "stringfuncs.h"
#include "vectorfuncs.h"

#include <vector>
#include <iostream>

static std::vector<unsigned char> key = random_aes_key();
static std::vector<unsigned char> IV = random_aes_key();

//same as ch16, but CTR not CBC
std::vector<unsigned char> func1(std::string input){
	std::string plaintext;
	//filter input
	std::string filtered;
	for (unsigned char c : input){
		if (c == ';' || c == '=')continue;
		filtered += c;
	}
	//prepend data
	std::string prepend = "comment1=cooking%20MCs;userdata=";
	plaintext = prepend + filtered;
	//append data
	std::string append = ";comment2=%20like%20a%20pound%20of%20bacon";
	plaintext = plaintext + append;
	//turn it into a vector for padding
	std::vector<unsigned char> plaintextVector = string_to_vector(plaintext);
	//pad to 16 byte (pkcs pad)
	std::vector<unsigned char> paddedPlaintextVector = PKCS_7_pad(plaintextVector, 16);
	//encrypt, and return
	return aes_ctr_encrypt(paddedPlaintextVector, key);
}
// ""
bool func2(std::vector<unsigned char> input){
	std::vector<unsigned char> plain = aes_ctr_decrypt(input, key);
	std::string plainString(plain.begin(), plain.end());

	return plainString.find(";admin=true;") != std::string::npos;

}

void challenge26(){
	//Since we are dealing with streams, only need to create known plaintext the size of the desired plaintext
	std::vector<unsigned char> adminBlock = string_to_vector(";admin=true;");
	std::vector<unsigned char> cipherText;
	std::string knownPlaintext;
	for (int i = 0; i < adminBlock.size();i++){
		knownPlaintext += 'A';
	}
	cipherText = func1(knownPlaintext);

	//same concept as before, get keystream, we get to write whatever data we like
	int count = 0;
	for (int j = 32; j < cipherText.size() && count<adminBlock.size(); j++){
		try{
			cipherText[j] = (unsigned char)'A' ^ (unsigned char)cipherText[j] ^ (unsigned char)adminBlock[count];
			count++;
			bool plainText = func2(cipherText);
		}
		catch (char* exception){
		}
	}
	if (func2(cipherText)) { std::cout << "win\n"; }
	else{ std::cout << "Lose!\n"; }
}