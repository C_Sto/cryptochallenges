#include "challenges.h"

#include "aes.h"
#include "vectorfuncs.h"

#include <vector>
#include <string>
#include <iostream>

static std::vector<unsigned char> key = random_aes_key();
static std::vector<unsigned char> IV = key;

static std::vector<unsigned char> func1(std::string input){
	std::string plaintext;
	//filter input
	std::string filtered;
	for (unsigned char c : input){
		if (c == ';' || c == '=')continue;
		filtered += c;
	}
	//prepend data
	std::string prepend = "comment1=cooking%20MCs;userdata=";
	plaintext = prepend + filtered;
	//append data
	std::string append = ";comment2=%20like%20a%20pound%20of%20bacon";
	plaintext = plaintext + append;
	//turn it into a vector for padding
	std::vector<unsigned char> plaintextVector = string_to_vector(plaintext);
	//pad to 16 byte (pkcs pad)
	std::vector<unsigned char> paddedPlaintextVector = PKCS_7_pad(plaintextVector, 16);
	//encrypt, and return
	return aes_cbc_encrypt(paddedPlaintextVector, key, IV, 128);
}

static std::string func2(std::vector<unsigned char> input){
	std::vector<unsigned char> plain = aes_cbc_decrypt(input, key, IV, 128);
	std::string plainString(plain.begin(), plain.end());
	for (unsigned char c : plain){
		if ( c < 32 || 
			c>127) 
			return plainString;
	}
	return "OK";

}


void challenge27(){
	/*
	theory:
	same iv as key means that encryption block 0 is xored against the key, and then sent to ECB.

	cbc means xor previous block against the plain/cipher-text before en/de-crypting.
	send 0's as block 2 means block 3 will be xor'd against 0's after being sent to the cipher engine - result will be block 3 decrypted with no xor
	xor plain with cipher to get IV, which == key. Win.
	
	*/

	//sender~~~~~~~~~~~
	std::vector<unsigned char> ciph = func1("A potentially Secret Message");
	//attacker~~~~~~~~~
	//split into chunks, for ease of use
	std::vector<std::vector<unsigned char>> chunks = chunker(ciph, 16);
	//modify as needed
	std::vector<unsigned char> zeroVec;
	for (int i = 0; i < 16; i++){
		zeroVec.push_back(0x00);
	}
	std::vector<unsigned char> editedCiphertext = join_Vectors(chunks[0], zeroVec);
	editedCiphertext = join_Vectors(editedCiphertext, chunks[0]);
	//send crafted message for decryption
	std::string response = func2(editedCiphertext);
	std::vector<unsigned char> attackerKey;
	//recover key
	if (response.compare("OK") != 0){
		std::vector<unsigned char> plainVec(response.begin(),response.end());
		std::vector<std::vector<unsigned char>> plainChunks = chunker(plainVec, 16);
		for (int i = 0; i < 16;i++){
			attackerKey.push_back(plainChunks[0][i] ^ plainChunks[2][i]);
		}
	}
	//see plaintext
	std::cout << vector_to_string(aes_cbc_decrypt(ciph, attackerKey, attackerKey, 128));
}