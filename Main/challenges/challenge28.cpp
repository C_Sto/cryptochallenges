#include "challenges.h"
#include "aes.h"

#include "hashing.h"
#include "vectorfuncs.h"

#include <iostream>
#include <string>

/*Find a SHA-1 implementation in the language you code in.

Don't cheat. It won't work.
Do not use the SHA-1 implementation your language already provides (for instance, don't use the "Digest" library in Ruby, or call OpenSSL; in Ruby, you'd want a pure-Ruby SHA-1).
Write a function to authenticate a message under a secret key by using a secret-prefix MAC, which is simply:

SHA1(key || message)
Verify that you cannot tamper with the message without breaking the MAC you've produced, and that you can't produce a new MAC without knowing the secret key.*/

bool verify_MAC(std::string hash, std::string in, std::vector<unsigned char> key){
	if (sha1(join_Vectors(key, in)).compare(hash) == 0) return true;
	return false;
}


void verify_MAC_output(std::string hash, std::string in, std::vector<unsigned char> key){
	std::cout << "Verifying message: " << in << '\n';
	std::cout << "Supplied hash: " << hash << '\n';
	std::cout << "Verification result: ";
	if (verify_MAC(hash, in, key)) std::cout << "True\n";
	else std::cout << "False\n";
	std::cout << '\n';
}

std::string get_MAC(std::string message, std::vector<unsigned char> pw){
	return sha1(join_Vectors(pw, message));
}

void challenge28(){
	//create message and 'secret' key
	std::vector<unsigned char> key = random_aes_key();
	std::string message = "authenticated message";
	std::string MAC = get_MAC(message, key);
	verify_MAC_output(MAC, message, key);

	//verify cannot tamper
	verify_MAC_output(MAC, "Authenticated as administrator", key);
	//verify can't produce new MAC without key
	//above is true, as mac verify function needs key input

	
}