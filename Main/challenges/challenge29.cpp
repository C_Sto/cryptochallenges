#include "challenges.h"

#include "hashing.h"
#include "vectorfuncs.h"

#include <vector>
#include <iostream>

static std::vector<unsigned char> key = string_to_vector("YELLOW SUBMARINE");

/*Secret-prefix SHA-1 MACs are trivially breakable.
The attack on secret-prefix SHA1 relies on the fact that you can take the ouput of SHA-1 and use it as a new starting point for SHA-1, thus taking an arbitrary SHA-1 hash and "feeding it more data".
Since the key precedes the data in secret-prefix, any additional data you feed the SHA-1 hash in this fashion will appear to have been hashed with the secret key.
To carry out the attack, you'll need to account for the fact that SHA-1 is "padded" with the bit-length of the message;
your forged message will need to include that padding. We call this "glue padding". The final message you actually forge will be:
SHA1(key || original-message || glue-padding || new-message)
(where the final padding on the whole constructed message is implied)
Note that to generate the glue padding, you'll need to know the original bit length of the message; the message itself is known to the attacker, but the secret key isn't, so you'll need to guess at it.
This sounds more complicated than it is in practice.
To implement the attack, first write the function that computes the MD padding of an arbitrary message
and verify that you're generating the same padding that your SHA-1 implementation is using. This should take you 5-10 minutes.
Now, take the SHA-1 secret-prefix MAC of the message you want to forge --- this is just a SHA-1 hash --- and break it into 32 bit SHA-1 registers (SHA-1 calls them "a", "b", "c", &c).
Modify your SHA-1 implementation so that callers can pass in new values for "a", "b", "c" &c (they normally start at magic numbers). With the registers "fixated", hash the additional data you want to forge.
Using this attack, generate a secret-prefix MAC under a secret key (choose a random word from /usr/share/dict/words or something) of the string:
"comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon"
Forge a variant of this message that ends with ";admin=true".
This is a very useful attack.
For instance: Thai Duong and Juliano Rizzo, who got to this attack before we did, used it to break the Flickr API.*/

std::vector<unsigned char> generate_pad(std::vector<unsigned char> in){
	//~~~~~~~~~~~~Pre-Processing~~~~~~~~~~~~~
	std::vector<unsigned char> ret;
	//get message size in bits
	int message_size = in.size() * 8;
	//message needs to be of size 448 mod 512 bits long
	int size_mod_512 = message_size % 512;
	int bits_to_add = size_mod_512 + 512;
	if (size_mod_512<448) bits_to_add = 448 - size_mod_512;
	if (size_mod_512 > 448) bits_to_add = 512 - (size_mod_512 - 448);
	//add '1' and 7 trailing 0's to message
	ret.push_back(0x80);
	bits_to_add -= 8;
	while (bits_to_add > 0){
		ret.push_back(0x00);
		bits_to_add -= 8;
	}
	//add 64 bit length to the end (big endian)
	unsigned int msg_size = message_size;
	for (int i = (64 - 8); i >= 0; i -= 8){
		unsigned char v = msg_size >> i;
		ret.push_back((unsigned char)(msg_size >> i));
	}
	return ret;
}


static bool verify_MAC(std::string hash, std::string in, std::vector<unsigned char> key){
	if (sha1(join_Vectors(key, in)).compare(hash) == 0) return true;
	return false;
}


static void verify_MAC_output(std::string hash, std::string in, std::vector<unsigned char> key){
	std::cout << "Verifying message: " << in << '\n';
	std::cout << "Supplied hash: " << hash << '\n';
	std::cout << "Verification result: ";
	if (verify_MAC(hash, in, key)) std::cout << "True\n";
	else std::cout << "False\n";
	std::cout << '\n';
}

static void attacker_verify_MAC_output(std::string hash, std::string in){
	std::cout << "Verifying message: " << in << '\n';
	std::cout << "Supplied hash: " << hash << '\n';
	std::cout << "Verification result: ";
	if (verify_MAC(hash, in, key)) std::cout << "True\n";
	else std::cout << "False\n";
	std::cout << '\n';
}

static bool attacker_verify_MAC(std::string hash, std::string in){
	return verify_MAC(hash, in, key);
}

static std::string get_MAC(std::string in){
	return sha1(join_Vectors(key, in));
}

void challenge29(){
	//get MAC (SHA1(key || msg ))
	std::string msg = "comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon";
	const std::string OG_MAC = get_MAC(msg); //SERVER SUPPLIES THIS -- attacker has not got access to this function, nor the key
	std::string msg2 = ";admin=true";
	//function to generate MD padding of 'any' arb message. Verify it works
	std::string glue = vector_to_string(generate_pad(string_to_vector(msg)));

	//take MAC of the OG message (which is just the hash) break into 32bit registers, (A,b,C,&C)
	std::string registers[5];
	for (int i = 0; i < 5; i++){
		registers[i] = OG_MAC.substr(0 + i * 8, 8);
	}

	//test area~~~~~~~~~~
	//test the legit msg - we want to get the same result for our own string.
	std::cout << "Legit:\n";
	attacker_verify_MAC_output(OG_MAC, msg);

	//forge message and MAC that conforms to secret key methods. sent msg = og msg + glue + extension

	std::string forged = msg + glue + msg2;
	std::cout << "Not legit:\n";
	int i = 0;
	//set SHA1 registers to ^, continue hashing additional data -use resulting hash as the MAC
	std::string constructed_MAC = sha1(string_to_vector(msg2), registers);
	std::string suspected_key_length;
	while (!attacker_verify_MAC(constructed_MAC,forged)){
		suspected_key_length += "0";
		glue = vector_to_string(generate_pad(string_to_vector(suspected_key_length + msg)));
		forged = msg+glue+msg2;
	}
	attacker_verify_MAC_output(constructed_MAC, forged);
}