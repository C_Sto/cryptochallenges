#include "challenges.h"

#include <vector>
#include <iostream>
#include <map>

#include "xor.h"
#include "stringfuncs.h"
#include "mapfuncs.h"

std::vector<unsigned char> test = readHex("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");

void challenge3(){
	std::map<unsigned char, double> mp = singleByte_xor_ScoreMap(test);
	std::cout << "Key: " << getLowestKey(mp) << '\n';
	std::vector<unsigned char> res = xor_SingleChar(test, getLowestKey(mp));
	std::string s(res.begin(), res.end());
	std::cout << "Message: " << s << '\n';
}