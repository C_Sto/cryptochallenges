#include "challenges.h"

#include <vector>
#include <iostream>
#include <string>
#include <tuple>

#include "fileio.h"
#include "stringfuncs.h"
#include "xor.h"

std::vector<std::vector<unsigned char>> hxList;

void challenge4(){
	for (std::string s : loadFile("challenges/challenge4.txt")){
		hxList.push_back(readHex(s));
	}
	std::cout << "Working.. please wait";
	std::tuple<std::string, unsigned char, std::string> tup = detectSingleByteXOr(hxList);
	std::cout << '\r';
	std::cout << "CipherText: \t" << std::get<0>(tup) << '\n';
	std::cout << "Key: \t\t" << std::get<1>(tup) << '\n';
	std::cout << "Plaintext: \t" << std::get<2>(tup) << '\n';
}