#include "challenges.h"

#include <string>
#include <vector>
#include <iostream>

#include "stringfuncs.h"
#include "xor.h"




std::string initCheck(){
	std::vector<unsigned char> v = readHex("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272"
		"a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f");
	std::string s(v.begin(), v.end());
	return s;
}

void challenge5(){
	std::string plainText = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
	std::string check = initCheck();
	std::cout << "Plain: " << '\n' << plainText << '\n';
	std::vector<unsigned char> cipherVect = repeatingKeyXOr(plainText, "ICE");
	std::string cipher(cipherVect.begin(), cipherVect.end());

	std::cout << "Cipher: " << '\n' << cipher << '\n';
	std::cout << "Check: " << '\n' << check << '\n';

	std::cout << "Compare: " << '\n' << check.compare(cipher) << '\n';
}