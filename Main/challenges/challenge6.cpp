#include "challenges.h"

#include <vector>
#include <iostream>

#include "stringfuncs.h"
#include "fileio.h"
#include "xor.h"
#include "vectorfuncs.h"
#include "mapfuncs.h"


void challenge6(){
	std::vector<unsigned char> ciphBytes = base64_decode(loadFileIntoString("challenges/challenge6.txt"));
	//get keysize
	int potentialKeySize = find_xor_KeySize(ciphBytes);

	std::cout << "Trying for keySize " << potentialKeySize << '\n';
	//break into blocks of keysise length
	std::vector<std::vector<unsigned char>>chunks = chunker(ciphBytes, potentialKeySize);
	//transpose the things
	std::vector<std::vector<unsigned char>>transposedChunks = transposeChunks(chunks);
	//solve each block as a single byte xor
	std::string pw;
	std::cout << "Cracking password " << potentialKeySize << " characters long..." << '\n';
	double ch = 0;
	for (std::vector<unsigned char>v : transposedChunks){
		std::map<unsigned char, double> m = singleByte_xor_ScoreMap(v);
		unsigned char c = getLowestKey(m);
		pw += c;
		std::cout << c << " " << (ch / (double)potentialKeySize) * 100 << "% Done.." << '\r';
		ch++;
	}
	std::cout << '\n' << "Done. Password is: " << pw << '\n';
	std::cout << "Decrypted text is: " << '\n';
	std::string ciphText(ciphBytes.begin(), ciphBytes.end());
	std::vector<unsigned char> pl = repeatingKeyXOr(ciphText, pw);
	std::string s(pl.begin(), pl.end());

	std::cout << s << '\n';
	std::cout << '\n';
}