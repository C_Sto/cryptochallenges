#include "challenges.h"

#include <vector>
#include <iostream>

#include "aes.h"
#include "stringfuncs.h"
#include "fileio.h"


void challenge7(){
	std::vector<unsigned char> ciph = base64_decode(loadFileIntoString("challenges/challenge7.txt"));
	std::vector<unsigned char> test = aes_ecb_decrypt(ciph, "YELLOW SUBMARINE", 128);
	std::string s(test.begin(), test.end());

	std::cout << s;

}