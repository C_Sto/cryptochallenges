#include "challenges.h"

#include <vector>
#include <iostream>

#include "fileio.h"
#include "stringfuncs.h"
#include "aes.h"



void challenge8(){
	std::vector<std::string> hexStrings = loadFile("challenges/challenge8.txt");
	double lowv = DBL_MAX;
	int ind = 0;
	int indv = 0;
	for (std::string s : hexStrings){
		if (is_ecb_mode(readHex(s), 16))
			std::cout << "ECB FOUND AT IND: " << ind << '\n';
		ind++;
	}
}