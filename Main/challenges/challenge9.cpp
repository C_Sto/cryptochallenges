#include "challenges.h"
#include <vector>
#include <iostream>
#include <string>

#include "vectorfuncs.h"

void challenge9(){
	std::string original = "YELLOW SUBMARINE";
	int padsize = 20;
	std::cout << "Before padding: "<< original<<'\n';
	std::vector<unsigned char> v(original.begin(), original.end());
	std::vector<unsigned char> r = PKCS_7_pad(v, padsize);
	
	std::string s(r.begin(),r.end());
	
	std::cout << "After padding with size " << padsize << ": " << s<<'\n';

}