#include "aes.h"

#include <vector>
#include <set>

#include "vectorfuncs.h"
#include "openssl\aes.h"
#include "openssl\err.h"
#include "xor.h"


std::vector<unsigned char> random_aes_key(){
	srand(time(NULL));
	std::vector<unsigned char> ret(16);
	for (int i = 0; i < 16; i++){
		ret.at(i) =((unsigned char)rand() % 255);
	}
	return ret;
}

void handleErrors(void){
	ERR_print_errors_fp(stderr);
	abort();
}
std::vector<unsigned char> aes_ecb_encrypt(const std::vector<unsigned char> plainTextEntry, const std::vector<unsigned char> key, int blockLen){
	std::vector<unsigned char> plainText;
	if (plainTextEntry.size() % (blockLen / 8) != 0){
		plainText = PKCS_7_pad(plainTextEntry, blockLen / 8);
	}
	else{
		plainText = plainTextEntry;
	}
	AES_KEY enc_key;
	AES_set_encrypt_key(&key[0], blockLen, &enc_key);
	int retVecSize = ceil((double)plainText.size() / (double)(blockLen / 8))*(blockLen / 8);
	std::vector<unsigned char> retVec(retVecSize);

	for (int i = 0; i < plainText.size(); i += (blockLen / 8)){
		AES_ecb_encrypt(&plainText[0] + i, &retVec[0] + i, &enc_key, AES_ENCRYPT);
	}
	return retVec;
}
std::vector<unsigned char> aes_ecb_encrypt(std::vector<unsigned char> plain, std::string key, int blockLen){
	const std::vector<unsigned char> s(key.begin(), key.end());
	return aes_ecb_encrypt(plain, s, blockLen);
}
std::vector<unsigned char> aes_ecb_encrypt(std::string plain, std::vector<unsigned char> key, int blockLen){
	const std::vector<unsigned char> s(plain.begin(), plain.end());
	return aes_ecb_encrypt(s, key, blockLen);

}

std::vector<unsigned char> aes_ecb_decrypt(const std::vector<unsigned char> cipher, const std::vector<unsigned char> key, int blockLen){
	AES_KEY dec_key;
	AES_set_decrypt_key(&key[0], blockLen, &dec_key);
	std::vector<unsigned char> retVec(16 * (ceil(cipher.size() / 16)));
	for (int i = 0; i < cipher.size(); i += (blockLen / 8)){
		AES_ecb_encrypt(&cipher[0] + i, &retVec[0] + i, &dec_key, AES_DECRYPT);

	}
	try{
		retVec = PKCS_7_unpad(retVec);
	}
	catch (char* c){
		throw c;
	}
	return retVec;
}
std::vector<unsigned char> aes_ecb_decrypt_no_pad(const std::vector<unsigned char> cipher, const std::vector<unsigned char> key, int blockLen){
	AES_KEY dec_key;
	AES_set_decrypt_key(&key[0], blockLen, &dec_key);
	std::vector<unsigned char> retVec(16 * (ceil(cipher.size() / 16)));
	for (int i = 0; i < cipher.size(); i += (blockLen / 8)){
		AES_ecb_encrypt(&cipher[0] + i, &retVec[0] + i, &dec_key, AES_DECRYPT);

	}
	return retVec;
}
std::vector<unsigned char> aes_ecb_decrypt(const std::string cipher, const std::string key, int blockLen){
	std::vector<unsigned char> ciphVec(cipher.begin(), cipher.end());
	std::vector<unsigned char> keyVec(key.begin(), key.end());
	return aes_ecb_decrypt(ciphVec, keyVec, blockLen);
}

std::vector<unsigned char> aes_ecb_decrypt(const std::vector<unsigned char> cipher, const std::string key, int blockLen){
	std::vector<unsigned char> ciphVec(cipher.begin(), cipher.end());
	std::vector<unsigned char> keyVec(key.begin(), key.end());
	return aes_ecb_decrypt(ciphVec, keyVec, blockLen);
}

std::vector<unsigned char> aes_cbc_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> iv, int blocksize){
	std::vector<std::vector<unsigned char>> plainChunks = chunker(in, blocksize / 8);
	std::vector<std::vector<unsigned char>> cipherChunks(plainChunks.size());
	std::vector<unsigned char> ret;
	//xor the first block against the IV
	cipherChunks[0] = xor_shortest(plainChunks[0], iv);
	//and then send it to the encryption engine
	cipherChunks[0] = aes_ecb_encrypt(cipherChunks[0], key, blocksize);
	//for the remaining blocks
	for (int i = 1; i < plainChunks.size(); i++){
		//xor against previous block
		cipherChunks[i] = xor_shortest(cipherChunks[i - 1], plainChunks[i]);
		//send to encryption engine
		cipherChunks[i] = aes_ecb_encrypt(cipherChunks[i], key, blocksize);

	}
	//reconstruct for exit
	for (std::vector<unsigned char> chunk : cipherChunks){
		for (unsigned char c : chunk){
			ret.push_back(c);
		}
	}
	return ret;
}
std::vector<unsigned char> aes_cbc_decrypt(std::vector<unsigned char> in, std::string key, std::vector<unsigned char> iv, int blocksize){
	// inverse of encrypt, basically
	std::vector<std::vector<unsigned char>> cipherChunks = chunker(in, blocksize / 8);
	std::vector<std::vector<unsigned char>> plainChunks(cipherChunks.size());
	std::vector<unsigned char> ret;
	for (int i = cipherChunks.size() - 1; i>0; i--){
		//send to encryption engine
		plainChunks[i] = aes_ecb_decrypt_no_pad(cipherChunks[i], string_to_vector(key), blocksize);
		//xor against previous block
		plainChunks[i] = xor_shortest(cipherChunks[i - 1], plainChunks[i]);
	}
	//send it to the encryption engine
	plainChunks[0] = aes_ecb_decrypt_no_pad(cipherChunks[0], string_to_vector(key), blocksize);
	//then xor the first block against the IV
	plainChunks[0] = xor_shortest(plainChunks[0], iv);


	//reconstruct for exit
	for (std::vector<unsigned char> chunk : plainChunks){
		for (unsigned char c : chunk){
			ret.push_back(c);
		}
	}
	return ret;
}
std::vector<unsigned char> aes_cbc_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> iv, int blocksize){
	return aes_cbc_decrypt(in, std::string(key.begin(), key.end()), iv, blocksize);
}
bool is_ecb_mode(const std::vector<unsigned char> ciph, int blocklen){
	std::vector<std::vector<unsigned char>> chunks = chunker(ciph, blocklen/8);
	std::set<std::vector<unsigned char>> yole(chunks.begin(), chunks.end());
	if (yole.size() != chunks.size())
		return true;
	return false;

}

std::vector<unsigned char> aes_ctr_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> nonce){
	std::vector<unsigned char> ret;
	std::vector<unsigned char> counterVector(8);
	long long counter = 0;
	unsigned char *counterPoint = static_cast<unsigned char*>(static_cast<void*>(&counter));
	std::vector<std::vector<unsigned char>> plainChunks = chunker(in, 16);
	//work block by block
	for (std::vector<unsigned char> plainChunk : plainChunks){
		//turn counter into workable vector
		counterVector._Construct(counterPoint, counterPoint + 8);
		//join nonce and counter
		std::vector<unsigned char> joined = join_Vectors(nonce, counterVector);
		//use ecb to encrypt the joined vectors with the key provided
		std::vector<unsigned char> keyStreamBlock = aes_ecb_encrypt(joined, key, 128);
		//xor each byte of key stream against input
		for (int i = 0; i < plainChunk.size(); i++){
			ret.push_back(keyStreamBlock[i] ^ plainChunk[i]);
		}
		counter++;

	}
	return ret;
}

std::vector<unsigned char> aes_ctr_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, long long nonce){
	unsigned char *ptr = static_cast<unsigned char*>(static_cast<void*>(&nonce));
	return aes_ctr_encrypt(in, key, std::vector<unsigned char>(ptr, ptr + 8));
}

std::vector<unsigned char> aes_ctr_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> nonce){
	return aes_ctr_encrypt(in, key, nonce);
}

std::vector<unsigned char> aes_ctr_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, long long nonce){
	return aes_ctr_encrypt(in, key, nonce);
}

std::vector<unsigned char> aes_ctr_encrypt(std::string in, std::vector<unsigned char> key, std::vector<unsigned char> nonce){
	return aes_ctr_encrypt(std::vector<unsigned char>(key.begin(), key.end()), key, nonce);
}

std::vector<unsigned char> aes_ctr_encrypt(std::string in, std::vector<unsigned char> key, long long nonce){
	unsigned char *ptr = static_cast<unsigned char*>(static_cast<void*>(&nonce));
	return aes_ctr_encrypt(in, key, std::vector<unsigned char>(ptr, ptr + 8));
}


std::vector<unsigned char> aes_ctr_edit(std::vector<unsigned char> ciphertext, std::vector<unsigned char> key, long offset, std::vector<unsigned char> newtext){
	ciphertext = aes_ctr_decrypt(ciphertext, key);
	ciphertext.erase(ciphertext.begin() + offset, ciphertext.begin() + offset + newtext.size());
	ciphertext.insert(ciphertext.begin() + offset, newtext.begin(), newtext.end());
	ciphertext = aes_ctr_encrypt(ciphertext, key);
	return ciphertext;

}