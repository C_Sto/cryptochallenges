#ifndef aes_h
#define aes_h

#include <vector>
#include <string>
#include <random>
#include <time.h>

//returns 16 byte key
std::vector<unsigned char> random_aes_key();
std::vector<unsigned char> aes_ecb_decrypt(const std::vector<unsigned char> cipher, const std::string key, int blockLen);
std::vector<unsigned char> aes_ecb_encrypt(const std::vector<unsigned char> plainTextString, std::vector<unsigned char> key, int blockLen);
std::vector<unsigned char> aes_ecb_encrypt(std::vector<unsigned char> plain, std::string key, int blockLen);
bool is_ecb_mode(const std::vector<unsigned char> ciph, int blocklen);
std::vector<unsigned char> aes_cbc_decrypt(std::vector<unsigned char> in, std::string key, std::vector<unsigned char> iv, int blocksize);
std::vector<unsigned char> aes_cbc_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> iv, int blocksize);
std::vector<unsigned char> aes_ecb_decrypt(const std::vector<unsigned char> cipher, const std::string key, int blockLen);
std::vector<unsigned char> aes_ecb_decrypt(const std::vector<unsigned char> cipher, const std::vector<unsigned char> key, int blockLen);

/*
@param in plaintext input
@param key key input
@param iv initialization vector
@param blocksize blocksize to encrypt to (in bits)
*/
std::vector<unsigned char> aes_cbc_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> iv, int blocksize);
std::vector<unsigned char> aes_ecb_encrypt(std::string plain, std::vector<unsigned char> key, int blockLenInBits);
std::vector<unsigned char> aes_ctr_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> nonce);
std::vector<unsigned char> aes_ctr_encrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, long long nonce = 0);
std::vector<unsigned char> aes_ctr_encrypt(std::string in, std::vector<unsigned char> key, std::vector<unsigned char> nonce);
std::vector<unsigned char> aes_ctr_encrypt(std::string in, std::vector<unsigned char> key, long long nonce = 0);
std::vector<unsigned char> aes_ctr_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, std::vector<unsigned char> nonce);
std::vector<unsigned char> aes_ctr_decrypt(std::vector<unsigned char> in, std::vector<unsigned char> key, long long nonce = 0);
std::vector<unsigned char> aes_ctr_edit(std::vector<unsigned char> ciphertext, std::vector<unsigned char> key, long offset, std::vector<unsigned char> newtext);

#endif