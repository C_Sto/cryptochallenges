#include "blackboxes.h"

#include <vector>
#include <random>
#include <ctime>
#include <string>

#include "aes.h"
#include "stringfuncs.h"
#include "vectorfuncs.h"

std::vector<unsigned char> encryption_blackBox1(std::vector<unsigned char> input){
	srand(std::time(0));

	//generate 'random' key
	std::vector<unsigned char> key(16);
	for (int i = 0; i < 16; i++){
		key[i] = (unsigned char)rand() % 255;
	}

	std::vector<unsigned char> ret;
	int r = rand() % 5 + 5;
	//append random before
	for (int i = 0; i < r; i++){
		input.push_back((unsigned char)rand() % 255);
	}
	//append random after
	r = rand() % 5 + 5;
	for (int i = 0; i < r; i++){
		input.insert(input.begin(), (unsigned char)rand() % 255);
	}
	//cbc half the time, ecb half the time
	int r2 = rand() % 2;
	if (r2 == 1){
		ret = aes_ecb_encrypt(input, key, 128);
	}
	else{
		//generate random iv
		std::vector<unsigned char> iv(16);
		for (int i = 0; i < 16; i++){
			iv[i] = (unsigned char)rand() % 255;
		}
		ret = aes_cbc_encrypt(input, key, iv, 128);
	}


	return ret;

}

std::vector<unsigned char> encryption_blackBox_12(std::vector<unsigned char>input){
	std::vector<unsigned char> key = { '\x04', '\x15', '\x05', '\x25', '\x07', '\x9', '\x05', '\xA', '\x04', '\x15', '\x05', '\x25', '\x07', '\x9', '\x05', '\xA' };
	std::vector<unsigned char> unknown_append = base64_decode(	"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg"
																"aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq"
																"dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK");
	std::vector<unsigned char> output;
	output.insert(output.begin(),input.begin(), input.end());
	output.insert(output.end(), unknown_append.begin(), unknown_append.end());
	output = aes_ecb_encrypt(output, key, 128);
	return output;
}

std::vector<unsigned char> encryption_blackBox_14(std::vector<unsigned char>input, int prependLen){
	srand(time(NULL));
	std::vector<unsigned char> key = { '\x04', '\x15', '\x05', '\x25', '\x07', '\x9', '\x05', '\xA', '\x04', '\x15', '\x05', '\x25', '\x07', '\x9', '\x05', '\xA' };
	std::vector<unsigned char> unknown_append = base64_decode("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg"
		"aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq"
		"dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK");
	//random prepend
	std::vector<unsigned char> output;
	for (int i = 0; i<prependLen; i++){
		output.push_back((unsigned char) rand() % 255);
	}
	output.insert(output.end(), input.begin(), input.end());
	output.insert(output.end(), unknown_append.begin(), unknown_append.end());
	output = aes_ecb_encrypt(output, key, 128);
	return output;
}

