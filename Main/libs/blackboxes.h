#ifndef blackboxes_h
#define blackboxes_h

#include <vector>

std::vector<unsigned char> encryption_blackBox1(std::vector<unsigned char> input);
std::vector<unsigned char> encryption_blackBox_12(std::vector<unsigned char> input);
std::vector<unsigned char> encryption_blackBox_14(std::vector<unsigned char> input, int padSize);

#endif