#include <string>
#include <vector>
#include <fstream>
#include "fileio.h"


std::string loadFileIntoString(std::string name){
	std::string ret;
	std::ifstream in;
	std::string str;
	in.open(name);
	while (std::getline(in, str)){
		ret += str;
	}
	return ret;
}
std::vector<std::string> loadFile(std::string name){
	std::vector<std::string> ret;
	std::ifstream in;
	std::string str;
	in.open(name);
	while (std::getline(in, str)){
		ret.push_back(str);
	}
	in.close();
	return ret;
}