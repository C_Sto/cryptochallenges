#ifndef fileio_h
#define fileio_h

#include <string>
#include <vector>

std::string loadFileIntoString(std::string name);
std::vector<std::string> loadFile(std::string name);
std::string toBase64(const std::vector<unsigned char> src, int len);

#endif