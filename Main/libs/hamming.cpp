#include <string>
#include <vector>
#include "hamming.h"


int hammingDistance(std::string str1, std::string str2){
	int r = 0;
	std::vector<unsigned char> v;
	for (int i = 0; i < str1.size(); i++){
		v.push_back(str1[i] ^ str2[i]);
	}
	for (unsigned char c : v){
		while (c){
			c &= (c - 1);
			r++;
		}
	}

	return r;
}
int hammingDistance(std::vector<unsigned char> v1, std::vector<unsigned char>v2){
	std::string s(v1.begin(), v1.end());
	std::string s2(v2.begin(), v2.end());
	return hammingDistance(s, s2);
}