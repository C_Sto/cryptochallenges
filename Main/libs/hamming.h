#ifndef hamming_h
#define hamming_h

#include <string>
#include <vector>

int hammingDistance(std::vector<unsigned char> v1, std::vector<unsigned char>v2);
int hammingDistance(std::string str1, std::string str2);

#endif