#include "hashing.h"

#include "vectorfuncs.h"

#include <sstream>
#include <iomanip>
#include <inttypes.h>
#include <iostream>

template < typename T>
std::string int_to_hex(T i){
	std::stringstream stream;
	stream << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex << i;
	return stream.str();
}

uint32_t rotl(uint32_t v, int shift){
	return ((v << shift) | (v >> (32 - shift)));
}

std::string sha1(std::vector<unsigned char> in, std::string A, std::string B, std::string C, std::string D, std::string E){
	//~~~~~~~~~~~~Pre-Processing~~~~~~~~~~~~~

	//get message size in bits
	int message_size = in.size() * 8;
	//message needs to be of size 448 mod 512 bits long
	int size_mod_512 = message_size % 512;
	int bits_to_add = size_mod_512 + 512;
	if (size_mod_512<448) bits_to_add = 448 - size_mod_512;
	if (size_mod_512 > 448) bits_to_add = 512 - (size_mod_512 - 448);
	//add '1' and 7 trailing 0's to message
	in.push_back(0x80);
	bits_to_add -= 8;
	while (bits_to_add > 0){
		in.push_back(0x00);
		bits_to_add -= 8;
	}
	//add 64 bit length to the end (big endian)
	uint64_t msg_size = message_size;
	for (int i = (64 - 8); i >= 0; i -= 8){
		unsigned char v = msg_size >> i;
		in.push_back((unsigned char)(msg_size >> i));
	}
	//bit length is now 0 modulo 512

	//APPEARS TO BE CORRECT TO HERE, UNSURE ABOUT BELOW
	//init registers
	uint32_t h[5];
	std::stringstream ss;
	ss << std::hex << A;
	ss >> h[0];
	ss.clear();
	ss << std::hex << B;
	ss >> h[1];
	ss.clear();
	ss << std::hex << C;
	ss >> h[2];
	ss.clear();
	ss << std::hex << D;
	ss >> h[3];
	ss.clear();
	ss << std::hex << E;
	ss >> h[4];
	ss.clear();

	//Chunk into 512 bit blocks
	std::vector<std::vector<unsigned char>> in_chunks = chunker(in, 64);
	//work on each chunk
	for (std::vector<unsigned char> chunk : in_chunks){
		uint32_t words_int[80];
		int count = 0;
		//break into sixteen 32 bit words
		for (int i = 0; i < chunk.size(); i += 4){
			words_int[count] = (chunk[0 + i] << 24) + (chunk[1 + i] << 16) + (chunk[2 + i] << 8) + chunk[3 + i];
			count++;
		}
		//extend to 80 32 bit words
		for (int k = 16; k < 80; k++){
			uint32_t pre_rot = (words_int[k - 3] ^ words_int[k - 8] ^ words_int[k - 14] ^ words_int[k - 16]);
			uint32_t post_rot = rotl(pre_rot, 1);
			words_int[k] = rotl(pre_rot, 1);
		}
		//init hash v for chunk
		uint32_t a = h[0];
		uint32_t b = h[1];
		uint32_t c = h[2];
		uint32_t d = h[3];
		uint32_t e = h[4];
		uint32_t k;
		uint32_t f;
		uint32_t temp;
		//main loop

		for (int l = 0; l < 80; l++){
			if (l < 20){
				f = (b & c) | ((~b) & d);
				k = 0x5A827999;
			}
			else if (l < 40){
				f = b ^ c ^ d;
				k = 0x6ED9EBA1;
			}
			else if (l < 60){
				f = (b & c) | (b & d) | (c & d);
				k = 0x8F1BBCDC;
			}
			else if (l < 80){
				f = b ^ c ^ d;
				k = 0xCA62C1D6;
			}

			temp = (rotl(a, 5)) + f + e + k + words_int[l];
			e = d;
			d = c;
			c = rotl(b, 30);
			b = a;
			a = temp;
		}
		//add chunk result to result
		h[0] += a;
		h[1] += b;
		h[2] += c;
		h[3] += d;
		h[4] += e;

	}
	return int_to_hex(h[0]) + int_to_hex(h[1]) + int_to_hex(h[2]) + int_to_hex(h[3]) + int_to_hex(h[4]);
}

std::string sha1(std::vector<unsigned char> in, std::string h[5]){
	return sha1(in, h[0], h[1], h[2], h[3], h[4]);
}

std::string sha1(std::vector<unsigned char> in){
	std::string h[5];
	h[0] = "0x67452301";
	h[1] = "0xEFCDAB89";
	h[2] = "0x98BADCFE";
	h[3] = "0x10325476";
	h[4] = "0xC3D2E1F0";
	return sha1(in, h);
}



