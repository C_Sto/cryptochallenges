#ifndef hashing_h
#define hashing_h

#include <string>
#include <vector>

std::string sha1(std::vector<unsigned char> in);
std::string sha1(std::vector<unsigned char> in, std::string A, std::string B, std::string C, std::string D);
std::string sha1(std::vector<unsigned char> in, std::string h[5]);

#endif