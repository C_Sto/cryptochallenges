#include <map>
#include "mapfuncs.h"

unsigned char getHighestKey(std::map<unsigned char, double> m){
	double topv = 0;
	unsigned char topk;
	for (const auto &pair : m){
		if (pair.second > topv){
			topv = pair.second;
			topk = pair.first;
		}
	}

	return topk;
}
unsigned char getLowestKey(std::map<unsigned char, double> m){
	double botv = DBL_MAX;
	unsigned char topk;
	for (const auto &pair : m){
		if (pair.second < botv){
			botv = pair.second;
			topk = pair.first;
		}
	}

	return topk;
}