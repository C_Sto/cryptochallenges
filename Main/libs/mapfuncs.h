#ifndef mapfuncs_h
#define mapfuncs_h

unsigned char getHighestKey(std::map<unsigned char, double> m);
unsigned char getLowestKey(std::map<unsigned char, double> m);


#endif