#include <vector>
#include <functional>
#include "aes.h"
#include "oracles.h"

/**
Accurately guesses if the given function is encrypting in ECB or CBC

@param func the function to test
@return returns 1 if it is en ECB mode, 0 if not (likely CBC)

*/
int aes_ecb_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>)> func){
	std::vector<unsigned char> trytext;
	for (int i = 1; i < 100; i++){
		trytext.push_back('\x41');
	}
	std::vector<unsigned char> check = func(trytext);
	return is_ecb_mode(check, 128);
}

int get_blocksize_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>)> func){
	std::vector<unsigned char> gen = { 'A' };
	std::vector < unsigned char > v = func(gen);
	int initSize = v.size();
	int vSize = initSize;
	while (initSize == vSize){
		gen.push_back('A');
		vSize = func(gen).size();
	}

	return (vSize - initSize) * 8;
}

int get_blocksize_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>, int)> func, int prependSize){
	std::vector<unsigned char> gen = { 'A' };
	std::vector < unsigned char > v = func(gen,prependSize);
	int initSize = v.size();
	int vSize = initSize;
	while (initSize == vSize || gen.size()>100){
		gen.push_back('A');
		vSize = func(gen, prependSize).size();
	}

	return (vSize - initSize);
}