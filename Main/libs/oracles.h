#ifndef oracles_h
#define oracles_h

#include <vector>
#include <functional>


int aes_ecb_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>)> func);
int get_blocksize_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>)> func);
int get_blocksize_oracle(std::function<std::vector<unsigned char>(std::vector<unsigned char>, int)> func, int padSize);


#endif