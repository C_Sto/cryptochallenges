#include "random.h"

	MT19937::MT19937(){
		init(0);
	}
	MT19937::MT19937(unsigned int i){
		init(i);
	}
	void MT19937::init(unsigned int seed = 0){
		index = 0;
		MT[0] = seed;
		for (int i = 1; i < MTSize; i++){
			unsigned long long v = 1812433253 * (MT[i - 1] ^ (MT[i - 1]) >> 30) + i;
			unsigned int n = v & ((1 << 32) - 1);
			MT[i] = n;
		}
	}
	unsigned int MT19937::get_number(){
		if (index == 0){ gen_numbers(); }

		unsigned int y = MT[index];
		y = y ^ (y >> 11);
		y = y ^ ((y << 7) & (2636928640));
		y = y ^ ((y << 15) & (4022730752));
		y = y ^ (y >> 18);
		index = (index + 1) % MTSize;
		return y;
	}
	void MT19937::gen_numbers(){
		for (int i = 0; i < MTSize; i++){
			unsigned int y = (MT[i] & 0x80000000) + (MT[(i + 1) % 624] & 0x7fffffff);
			MT[i] = MT[(i + 397) % MTSize] ^ (y >> 1);
			if ((y % 2) != 0) MT[i] = MT[i] ^ (2567483615);
		}

	}

