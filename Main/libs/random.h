#ifndef random_h
#define random_h

class MT19937{
	int MTSize = 624;
	unsigned int MT[624];
	int index = 0;
	void init(unsigned int seed);

public:
	unsigned int get_number();
	MT19937();
	MT19937(unsigned int seed);
	void gen_numbers();

	void splice(unsigned int v[]){
		for (int i = 0; i < MTSize; i++) MT[i] = v[i];
	}

};


#endif