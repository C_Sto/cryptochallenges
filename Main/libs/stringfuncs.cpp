#include <vector>
#include <string>
#include <algorithm>
#include "stringfuncs.h"

std::vector<unsigned char> readHex(std::string src){
	std::vector<unsigned char> ret;
	for (int i = 0; i < src.length(); i += 2){
		unsigned char x = stoul(src.substr(i, 2), nullptr, 16);
		ret.push_back(x);
	}
	return  ret;
}
std::vector<unsigned char> base64_decode(std::string const& encoded_string) {
	const std::string base64_chars =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"0123456789+/";
	int in_len = encoded_string.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	std::vector<unsigned char> ret;

	while (in_len-- && (encoded_string[in_] != '=')) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i == 4) {
			for (i = 0; i <4; i++)
				char_array_4[i] = base64_chars.find(char_array_4[i]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				ret.push_back(char_array_3[i]);
			i = 0;
		}
	}

	if (i) {
		for (j = i; j <4; j++)
			char_array_4[j] = 0;

		for (j = 0; j <4; j++)
			char_array_4[j] = base64_chars.find(char_array_4[j]);

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
	}

	return ret;
}
std::string base64_encode(const std::vector<unsigned char> src){
	int len = src.size();
	const std::string base64_chars =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"0123456789+/";
	std::string ret;
	int i = 0, j = 0;
	unsigned char block_3[3];
	unsigned char block_4[4];
	unsigned char const* bytesLocation = &src[0];
	while (len--){
		block_3[i++] = *(bytesLocation++);
		if (i == 3){
			block_4[0] = (block_3[0] & 0xfc) >> 2;
			block_4[1] = ((block_3[0] & 0x03) << 4) + ((block_3[1] & 0xf0) >> 4);
			block_4[2] = ((block_3[1] & 0x0f) << 2) + ((block_3[2] & 0xc0) >> 6);
			block_4[3] = block_3[2] & 0x3f;

			for (i = 0; i < 4; i++)
				ret += base64_chars[block_4[i]];
			i = 0;
		}
	}
	if (i){
		for (j = i; j < 3; j++)
			block_3[j] = '\0';
		block_4[0] = (block_3[0] & 0xfc) >> 2;
		block_4[1] = ((block_3[0] & 0x03) << 4) + ((block_3[1] & 0xf0) >> 4);
		block_4[2] = ((block_3[1] & 0x0f) << 2) + ((block_3[2] & 0xc0) >> 6);
		block_4[3] = block_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[block_4[j]];
		while ((i++ < 3))
			ret += '=';
	}


	return ret;
}
std::string repeatToLength(std::string init, int len){
	std::string ret = init;
	int ind = 0;
	while (ret.size() != len){
		ret += ret[ind];
		ind++;
	}
	return ret;
}


bool invalid_char(unsigned char c){
	return !isprint((unsigned)c);
}

std::string strip_non_print(std::string input){
	input.erase(remove_if(input.begin(), input.end(), invalid_char), input.end());
	return input;
}
