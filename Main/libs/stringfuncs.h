#ifndef stringfuncs_h
#define stringfuncs_h

#include <string>
#include <vector>

std::vector<unsigned char> readHex(std::string src);
std::vector<unsigned char> base64_decode(std::string const& encoded_string);
std::string base64_encode(const std::vector<unsigned char> src);
std::string repeatToLength(std::string init, int len);
std::vector<unsigned char> readHex(std::string src);
std::string strip_non_print(std::string input);
bool invalid_char(unsigned char c);

#endif