#include "textanalysis.h"
#include <map>
#include <string>
#include <vector>
#include <algorithm>

std::map<char, double> initMap(){
	std::map<char, double> freq;
	freq['a'] = 8.167; freq['b'] = 1.492; freq['c'] = 2.782; freq['d'] = 4.253; freq['e'] = 12.702;
	freq['f'], freq['g'], freq['h'], freq['i'], freq['j'] = 2.228, 2.015, 6.094, 6.966, 0.153;
	freq['k'], freq['l'], freq['m'], freq['n'], freq['o'] = 0.772, 4.025, 2.406, 6.749, 7.507;
	freq['p'], freq['q'], freq['r'], freq['s'], freq['t'] = 1.929, 0.095, 5.987, 6.327, 9.056;
	freq['u'], freq['v'], freq['w'], freq['x'], freq['y'] = 2.758, 0.978, 2.360, 0.150, 1.974;
	freq['z'], freq[' '], freq['\n'] = 0.074, 15.00, 0.01;
	return freq;
}
std::map<char, double> freq = initMap();
double chiSquare(std::string text, char letter){
	double letCount = std::count(text.begin(), text.end(), letter);
	double expected = 0;
	if (ispunct(letter) || isdigit(letter))
		expected = (0.10 / 100.0)*(text.size());
	else if (freq.count(letter) > 0)
		expected = (freq.at(letter) / 100)*text.size();
	else
		expected = text.size()*0.0001;
	if (expected == 0)
		return 0;
	return(pow((letCount - expected), 2) / expected);
}
double scorePlaintext(std::string s){
	char ca[] = "abcdefghijklmnopqrstuvwxyz \n";
	double tot = 0;
	double val = 0;
	std::vector<char> tested;
	std::vector<char> testable(std::begin(ca), std::end(ca));
	std::string scop(s);
	std::transform(scop.begin(), scop.end(), scop.begin(), ::tolower);
	for (char c : testable){

		val = chiSquare(scop, c);
		tot += val;
		tested.push_back(c);
	}

	for (unsigned char c : s){
		if (std::find(tested.begin(), tested.end(), c) != tested.end())
			continue;
		if (!isprint(c) && c != '\n')
			tot += 200;
		else{
			tot += chiSquare(scop, c);
		}
		tested.push_back(c);
	}

	return tot;
}