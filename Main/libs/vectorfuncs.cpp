#include <vector>
#include <algorithm>
#include "vectorfuncs.h"
#include <iostream>


std::vector<std::vector<unsigned char>>chunker(std::vector<unsigned char> in, int size){
	std::vector<std::vector<unsigned char>> ret;
	std::vector<unsigned char>::iterator start, end;
	start = end = in.begin();
	while (end != in.end()){
		int step = std::min(size, std::distance(end, in.end()));
		std::advance(end, step);
		std::vector<unsigned char> aux(end - start);
		std::copy(start, end, aux.begin());
		std::advance(start, step);
		ret.push_back(aux);
	}
	return ret;

}

std::vector<std::vector<unsigned char>> transposeChunks(std::vector<std::vector<unsigned char>> in){
	std::vector<std::vector<unsigned char>> ret;
	for (int i = 0; i < in[0].size(); i++){
		std::vector<unsigned char> v;
		ret.push_back(v);
		for (std::vector<unsigned char> chunk : in){
			if (chunk.size()>i){
				ret[i].push_back(chunk[i]);
			}
		}
	}
	return ret;
}

std::vector<unsigned char> PKCS_7_pad(std::vector<unsigned char> in, int len_in_bytes){
	int blocks = ceil(in.size() / (double)len_in_bytes)*len_in_bytes;
	unsigned char pad = blocks-in.size();
	if (in.size() % len_in_bytes == 0){
		for (int i = 0; i < 16; i++){
			in.push_back((unsigned char)16);
		}
	}
	else{
		while (in.size() % len_in_bytes != 0){
			in.push_back(pad);
		}
	}
	return in;
}

std::vector<unsigned char> PKCS_7_unpad(std::vector<unsigned char> in){
	std::vector<unsigned char> ret;
	//check for padding
	int i = in.size() - 1;
	unsigned char possible_pad = in.at(i);
	int padSize = possible_pad;
	if (padSize > 16|| padSize == 0)throw "Bad Padding";
	for (int i = in.size() - 1; i > in.size() - padSize; i--){
		if (in.at(i - 1) != possible_pad){
			throw "Bad Padding";
			return in;
		}
	}
	ret = std::vector<unsigned char>(in.begin(), in.end() - padSize);

	return ret;
	
}

bool has_block_repeats(std::vector<unsigned char> testVec, int blockSize){
	std::vector<std::vector<unsigned char>> chunks = chunker(testVec, blockSize);

	for (int i = 0; i < chunks.size();i++){
		std::vector<unsigned char> testChunk = chunks[i];
		for (int j = 0; j < chunks.size(); j++){
			if (i == j)continue;
			if (testChunk == chunks[j])return true;
		}
	}

	return false;
}

std::vector<unsigned char> join_Vectors(std::vector<unsigned char> v1, std::vector<unsigned char> v2){
	std::vector<unsigned char> ret;
	ret.reserve(v1.size()+ v2.size());
	ret.insert(ret.end(), v1.begin(), v1.end());
	ret.insert(ret.end(), v2.begin(), v2.end());
	return ret;
}

std::vector<unsigned char> join_Vectors(std::vector<unsigned char> v1, std::string v2){ 
	return join_Vectors(v1, std::vector<unsigned char>(v2.begin(), v2.end()));
}
std::vector<unsigned char> join_chunks(std::vector<std::vector<unsigned char>> chunks){
	std::vector<unsigned char> ret;
	for (int i = 0; i < chunks.size(); i++){
		ret = join_Vectors(ret,chunks[i]);
	}

	return ret;

}
std::vector<unsigned char> string_to_vector(std::string input){
	std::vector<unsigned char> ret;
	for (unsigned char c : input){
		ret.push_back(c);
	}

	return ret;
}

std::string vector_to_string(std::vector<unsigned char> input){
	return std::string(input.begin(), input.end());

}