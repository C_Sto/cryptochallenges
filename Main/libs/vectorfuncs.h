#ifndef vectorfuncs_h
#define vectorfuncs_h

#include <vector>

std::vector<std::vector<unsigned char>>chunker(std::vector<unsigned char> in, int size);
std::vector<std::vector<unsigned char>> transposeChunks(std::vector<std::vector<unsigned char>> in);
std::vector<unsigned char> PKCS_7_pad(std::vector<unsigned char> in, int len);
std::vector<unsigned char> PKCS_7_unpad(std::vector<unsigned char> in);
bool has_block_repeats(std::vector<unsigned char> testVec, int blockSize);
std::vector<unsigned char> join_Vectors(std::vector<unsigned char> v1, std::vector<unsigned char> v2);
std::vector<unsigned char> join_Vectors(std::vector<unsigned char> v1, std::string v2);
std::vector<unsigned char> string_to_vector(std::string input);
std::string vector_to_string(std::vector<unsigned char> input);
std::vector<unsigned char> join_chunks(std::vector<std::vector<unsigned char>> chunks);
#endif