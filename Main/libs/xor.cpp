#include <vector>
#include <string>
#include <map>

#include "textanalysis.h"
#include "mapfuncs.h"
#include "stringfuncs.h"
#include "hamming.h"

#include "xor.h"

std::vector<unsigned char> xor_SameSize(std::vector<unsigned char> v1, std::vector<unsigned char> v2){
	int size = v1.size();
	std::vector<unsigned char> ret;
	if (v2.size() > v1.size())
		size = v2.size();
	for (int i = 0; i < size; i++){
		ret.push_back(v1[i] ^ v2[i]);
	}

	return ret;
}
std::vector<unsigned char> xor_shortest(std::vector<unsigned char>v1, std::vector<unsigned char>v2){
	if (v1.size() == v2.size()){return xor_SameSize(v1, v2);}
	std::vector<unsigned char>temp;
	if (v1.size()>v2.size()){ 
		temp = std::vector<unsigned char>(v1.begin(), v1.end()-(v1.size()-v2.size()));
		return xor_SameSize(temp, v2);
	}
	if (v1.size() < v2.size()){
		temp = std::vector<unsigned char>(v2.begin(), v2.end() - (v2.size() - v1.size()));
		return xor_SameSize(temp, v1);
	}
}
std::vector<unsigned char> xor_longest(std::vector<unsigned char> v1, std::vector<unsigned char> v2){
	std::vector<unsigned char> ret;
	int v1Size = v1.size();
	int v2Size = v2.size();
	if (v1Size == v2Size)return xor_SameSize(v1, v2);
	if (v1Size > v2Size){
		ret = xor_SameSize(std::vector<unsigned char>(v1.begin(), v1.end() - (v1Size - v2Size)),v2);
		ret.insert(ret.end(), v1.begin() + v2Size, v1.end());
	}
	else{
		ret = xor_SameSize(std::vector<unsigned char>(v2.begin(), v2.end() - (v2Size - v1Size)), v1);
		ret.insert(ret.end(), v2.begin()+v1Size, v2.end());
	}
	return ret;
}
std::vector<unsigned char> xor_SingleChar(std::vector<unsigned char> v, unsigned char c){
	int size = v.size();
	std::vector<unsigned char> ret;
	for (int i = 0; i < size; i++){
		ret.push_back(v[i] ^ c);
	}
	return ret;
}

std::map<unsigned char, double> singleByte_xor_ScoreMap(std::vector<unsigned char> v){
	std::map<unsigned char, double> ret;
	for (int i = 0; i < 256; i++){
		std::vector<unsigned char> tempV = xor_SingleChar(v, (unsigned char)i);
		std::string s(tempV.begin(), tempV.end());
		ret[i] = scorePlaintext(s);
	}

	return ret;
}

std::tuple<unsigned char, double> scoreSingleByteXOr(std::vector<unsigned char> in){
	std::map<unsigned char, double> mp = singleByte_xor_ScoreMap(in);
	unsigned char lk = getLowestKey(mp);
	return std::tuple<unsigned char, double>(lk, mp.at(lk));

}

std::tuple<std::string, unsigned char, std::string> detectSingleByteXOr(std::vector<std::vector<unsigned char>> in){
	std::tuple<std::string, unsigned char, std::string> ret;
	std::string temCiph;
	char temChar;
	std::string temPlain;
	double lowScore = DBL_MAX;
	for (std::vector<unsigned char> vec : in){
		std::tuple<unsigned char, double> tup = scoreSingleByteXOr(vec);
		if (std::get<1>(tup) < lowScore){
			lowScore = std::get<1>(tup);
			temCiph = std::string(vec.begin(), vec.end());
			temChar = std::get<0>(tup);
			std::vector<unsigned char> v1 = xor_SingleChar(vec, temChar);
			temPlain = std::string(v1.begin(), v1.end());
		}
		double indx = std::find(in.begin(), in.end(), vec) - in.begin();
		double done = (indx / in.size()) * 100;
	}
	return std::make_tuple(temCiph, temChar, temPlain);

}

std::vector<unsigned char> repeatingKeyXOr(std::string plain, std::string key){
	std::vector<unsigned char> ret;
	std::string makeKey = repeatToLength(key, plain.size());
	for (int i = 0; i < plain.size(); i++){
		ret.push_back((plain[i] ^ makeKey[i]));
	}

	return ret;
}

int find_xor_KeySize(std::vector<unsigned char> in){
	double ret = DBL_MAX;
	int reti;
	//try a few keysizes
	for (int keySize = 2; keySize < 41; keySize++){
		//get first keySize worth of bytes
		std::vector<unsigned char> first;
		for (int i = 0; i < keySize * 4; i++){
			first.push_back(in[i]);
		}
		std::vector<unsigned char> second;
		//get second keySize worth of bytes
		for (int i = keySize * 3; i < keySize * 4 * 2; i++){
			second.push_back(in[i]);
		}
		//calculate hamming distance
		double dist = hammingDistance(first, second);
		//normalize
		dist = dist / keySize * 2;
		if (dist < ret){
			ret = dist;
			reti = keySize;
		}
	}
	return reti;
}