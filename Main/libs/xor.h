#ifndef xor_h
#define xor_h

#include <vector>
#include <map>
#include <string>
std::vector<unsigned char> xor_shortest(std::vector<unsigned char>v1, std::vector<unsigned char>v2);
std::vector<unsigned char> xor_SingleChar(std::vector<unsigned char> v, unsigned char c);
std::vector<unsigned char> xor_longest(std::vector<unsigned char> v1, std::vector<unsigned char> v2);
std::map<unsigned char, double> singleByte_xor_ScoreMap(std::vector<unsigned char> v);
std::tuple<std::string, unsigned char, std::string> detectSingleByteXOr(std::vector<std::vector<unsigned char>> in);
std::vector<unsigned char> repeatingKeyXOr(std::string plain, std::string key);
int find_xor_KeySize(std::vector<unsigned char> in);

#endif